﻿public enum TileStateEnum
{
	Clear = 0,
	Busy,
	Bonus,
	BonusX2,
	Void,
	Attention
}
