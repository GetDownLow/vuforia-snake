﻿using UnityEngine;
using System.Collections;

public class ParticleObj :MonoBehaviour
{
	private ParticleSystem partSystem;

	void Awake()
	{
		partSystem = GetComponent<ParticleSystem>();
	}

	public void OnEnable()
	{
		StartCoroutine(pushPart());
	}

	private IEnumerator pushPart()
	{
		yield return new WaitForSeconds(partSystem.duration);
		gameObject.SetActive(false);	
	}
}