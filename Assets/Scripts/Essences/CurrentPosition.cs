﻿public struct CurrentPosition
{
	public int X;

	public int Z;

	public CurrentPosition(int x, int z)
	{
		X = x;
		Z = z;
	}
}
