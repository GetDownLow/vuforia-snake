﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
	/// <summary>
	/// Aviable to move
	/// </summary>
	[HideInInspector]
	public TileStateEnum TileState;

	/// <summary>
	/// Tile position
	/// </summary>
	[HideInInspector]
	public Transform CachedTransform;

	/// <summary>
	/// Bonus object
	/// </summary>
	[HideInInspector]
	public GameObject Bonus;

	/// <summary>
	/// Wall animation
	/// </summary>
	[HideInInspector]
	public Animation Animation;

	public void RemoveBonus()
	{
		PoolManager.Instance.Pop(TileState == TileStateEnum.Bonus ? 3 : 5, CachedTransform.position, Quaternion.identity);
		Bonus.SetActive(false);
		Bonus = null;
	}

	public void ResetPos()
	{
		CachedTransform.GetChild(0).localPosition = new Vector3(0, -1, 0);
	}

	private void SetObstacle()
	{
		TileState = TileStateEnum.Busy;
	}

	private void SettFree()
	{
		TileState = TileStateEnum.Clear;
	}
}
