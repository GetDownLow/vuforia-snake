﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AudioBtn : MonoBehaviour
{
	[SerializeField] private string _audioName;

	private void Start ()
	{
		GetComponent<Button>().onClick.AddListener(() => AudioManager.Instance.PlayConstantSound(_audioName));
	}

}
