﻿using UnityEngine;

class Body : MonoBehaviour, ISnake
{
	public Transform CachedTransform { get; set; }
	public CurrentPosition CurrentPos { get; set; }
	public MeshFilter Mesh { get; set; }

	private void Awake()
	{
		CachedTransform = GetComponent<Transform>();
		Mesh = GetComponent<MeshFilter>();
	}
}
