﻿using UnityEngine;

public static class RangeUtility
{
	public static float CalcDistance(Vector3 a, Vector3 b)
	{
		return (new Vector2(a.x, a.z) - new Vector2(b.x, b.z)).magnitude;
	}

	public static float CalcDistance(CurrentPosition pos1, CurrentPosition pos2)
	{
		return (new Vector2(pos1.X, pos1.Z) - new Vector2(pos1.X, pos1.Z)).magnitude;
	}
}