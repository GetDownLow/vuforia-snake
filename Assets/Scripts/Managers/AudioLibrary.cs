﻿using System.Collections.Generic;
using UnityEngine;

public class AudioLibrary : MonoBehaviour
{
	[SerializeField]
	private Sound[] sounds;


	private Dictionary<string, AudioClip> soundsDict = new Dictionary<string, AudioClip>();

	private void Awake()
	{
		for (int i = 0; i < sounds.Length; i++)
		{
			soundsDict.Add(sounds[i].Name, sounds[i].Audio);
		}
	}

	public AudioClip GetSound(string id)
	{
		if (soundsDict.ContainsKey(id))
		{
			return soundsDict[id];
		}
		return null;
	}
}

[System.Serializable]
public class Sound
{
	public string Name;
	public AudioClip Audio;
}
