﻿using UnityEngine;

public class CollectableManager : MonoBehaviour
{
	public static CollectableManager Instance;

	private void Awake ()
	{
		Instance = this;
	}
	
	public void Spawn()
	{
		var freeTile = MapManager.Instance.GetFreeTile();
		var luck = LuckyRuby(20);
		freeTile.TileState = luck ? TileStateEnum.BonusX2 : TileStateEnum.Bonus;
		freeTile.Bonus = PoolManager.Instance.Pop(luck ? 4 : 1, freeTile.CachedTransform.position, Quaternion.identity);
	}

	private bool LuckyRuby(int precents)
	{
		return Random.Range(0f, 100f) <= precents;
	}
}
