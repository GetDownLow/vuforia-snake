﻿ using UnityEngine;
using System.Collections;
 using System.Collections.Generic;
 using UnityEngine.SceneManagement;

public class SnakeControl : MonoBehaviour
{
	public static SnakeControl Instance;

	#region Snake Serialized Params

	[Header("Snake params")]

	/// <summary>
	/// Delay between move of snake
	/// </summary>
	[SerializeField]
	private float _moveDelay;

	[SerializeField]
	private Mesh _bodyMesh;

	[SerializeField]
	private Mesh _tailMesh;

	#endregion

	#region Current Values
	/// <summary>
	/// Current direction of snake
	/// </summary>
	private DirectionEnum _currentDir;

	/// <summary>
	/// Direction for next move
	/// </summary>
	private DirectionEnum _nextDir;

	private float _currentDelay;

	private bool _needToGrow;

	private CurrentPosition _currentPos;
	#endregion

	/// <summary>
	/// Pieces of snake
	/// </summary>
	private List<ISnake> _snakePieces = new List<ISnake>();

	[HideInInspector]
	public ISnake Head;

	private void Awake ()
	{
		Instance = this;
	}

	public void SetSpeed(int cellPerSecond)
	{
		_moveDelay = 1f / cellPerSecond;
	}

	public void InitializeComponents()
	{
		_snakePieces.Clear();
		_currentDelay = 0;
		_needToGrow = false;
		_currentDir = _nextDir = DirectionEnum.Right;
		_currentPos = MapManager.Instance.GetMiddleOfMap();
		StartSpawn(2);
	}

	private void StartSpawn(int additionalPieces)
	{
		Head = PoolManager.Instance.Pop(2, MapManager.Instance.TakePosition(_currentPos), Quaternion.identity).GetComponent<ISnake>();
		// start rotate head
		Head.CachedTransform.forward = new Vector3(Head.CachedTransform.position.x + 1, 
			Head.CachedTransform.position.y, Head.CachedTransform.position.z) - Head.CachedTransform.position;
		Head.CurrentPos = _currentPos;
		_snakePieces.Add(Head);
		for (int i = 0; i < additionalPieces; i++)
		{
			var newPos = new CurrentPosition(_currentPos.X - i - 1, _currentPos.Z);
			var body = PoolManager.Instance.Pop(0, MapManager.Instance.TakePosition(newPos), Quaternion.identity).GetComponent<ISnake>();
			body.CurrentPos = newPos;
			body.Mesh.sharedMesh = _bodyMesh;
			_snakePieces.Add(body);
		}
		_snakePieces[_snakePieces.Count - 1].Mesh.sharedMesh = _tailMesh;
		_snakePieces[_snakePieces.Count - 1].CachedTransform.forward  = 
			_snakePieces[_snakePieces.Count - 2].CachedTransform.position -
			_snakePieces[_snakePieces.Count - 1].CachedTransform.position;
	}

	private void Update ()
	{
		if (GameControl.Instance.GameState != GameStateEnum.Game)
		{
			return;
		}
		_currentDelay += Time.deltaTime;
		if (_currentDelay > _moveDelay)
		{
			Move();
			_currentDelay = 0;
		}
	}

	private void Move()
	{
		Step();
		// clear last pos, if don't need to grow
		if (!_needToGrow)
		{
			MapManager.Instance.FreePosition(_snakePieces[_snakePieces.Count - 1].CurrentPos);
		}
		// check aviable next move or bonus
		var tileState = MapManager.Instance.CheckTileAvailability(_currentPos);
		switch (tileState)
		{
			case TileStateEnum.Bonus:
			case TileStateEnum.BonusX2:
				MapManager.Instance.GetTile(_currentPos).RemoveBonus();
				PointsManager.Instance.AddPoint(tileState == TileStateEnum.Bonus ? 1 : 2);
				AudioManager.Instance.PlayConstantSound(tileState == TileStateEnum.Bonus ? "Bonus" : "Bonus2");
				_needToGrow = true;
				break;
			case TileStateEnum.Void:
			case TileStateEnum.Busy:
				GameControl.Instance.GameOver();
				return;
		}
		CurrentPosition tmpPos = Head.CurrentPos;
		// move head
		Head.CachedTransform.position = MapManager.Instance.TakePosition(_currentPos);
		// rotate
		Head.CachedTransform.forward = MapManager.Instance.GetTile(_currentPos).CachedTransform.position -
					MapManager.Instance.GetTile(tmpPos).CachedTransform.position;
		// save postion
		Head.CurrentPos = _currentPos;
		// move every piece
		tmpPos = MovePieces(tmpPos);
		// spawn new part of body, if need
		if (_needToGrow)
		{
			AddBody(tmpPos);
			CollectableManager.Instance.Spawn();
		}
	}

	private CurrentPosition MovePieces(CurrentPosition tmpPos)
	{
		for (int i = 1; i < _snakePieces.Count; i++)
		{
			var temp = _snakePieces[i].CurrentPos;
			// move
			_snakePieces[i].CachedTransform.position = MapManager.Instance.GetTile(tmpPos).CachedTransform.position;
			// rotate
			_snakePieces[i].CachedTransform.forward = _snakePieces[i - 1].CachedTransform.position -
				 _snakePieces[i].CachedTransform.position;
			_snakePieces[i].CurrentPos = tmpPos;
			tmpPos = temp;
		}
		return tmpPos;
	}

	private void AddBody(CurrentPosition tmpPos)
	{
		_snakePieces[_snakePieces.Count - 1].Mesh.sharedMesh = _bodyMesh;
		var tail = PoolManager.Instance.Pop(0, MapManager.Instance.TakePosition(tmpPos), Quaternion.identity).GetComponent<ISnake>();
		// rotate to last body piece
		tail.CachedTransform.forward = _snakePieces[_snakePieces.Count - 1].CachedTransform.position -
			tail.CachedTransform.position;
		tail.CurrentPos = tmpPos;
		tail.Mesh.sharedMesh = _tailMesh;
		_snakePieces.Add(tail);
		_needToGrow = false;
	}

	/// <summary>
	/// Make step based on current direction
	/// </summary>
	private void Step()
	{
		switch (_nextDir)
		{
			case DirectionEnum.Backward:
				_currentPos = new CurrentPosition(_currentPos.X, _currentPos.Z - 1);
				break;
			case DirectionEnum.Forward:
				_currentPos = new CurrentPosition(_currentPos.X, _currentPos.Z + 1);
				break;
			case DirectionEnum.Right:
				_currentPos = new CurrentPosition(_currentPos.X + 1, _currentPos.Z);
				break;
			case DirectionEnum.Left:
				_currentPos = new CurrentPosition(_currentPos.X - 1, _currentPos.Z);
				break;
		}
		_currentDir = _nextDir;
	}

	/// <summary>
	/// Set next direction based on input and current direction
	/// </summary>
	/// <param name="toLeft"></param>
	public void Turn(bool toLeft)
	{
		switch (_currentDir)
		{
			case DirectionEnum.Backward:
				_nextDir = toLeft ? DirectionEnum.Right : DirectionEnum.Left;
				break;
			case DirectionEnum.Forward:
				_nextDir = toLeft ? DirectionEnum.Left : DirectionEnum.Right;
				break;
			case DirectionEnum.Right:
				_nextDir = toLeft ? DirectionEnum.Forward : DirectionEnum.Backward;
				break;
			case DirectionEnum.Left:
				_nextDir = toLeft ? DirectionEnum.Backward : DirectionEnum.Forward;
				break;
		}
	}
}
