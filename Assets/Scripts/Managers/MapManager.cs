﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class MapManager : MonoBehaviour
{
	public static MapManager Instance;

	[HideInInspector] private Tile[,] _tiles;

	[SerializeField] private Transform _parentObject;

	[SerializeField] private Material _oddBlockMaterial;
	[SerializeField] private Material _evenBlockMaterial;

	[SerializeField] private GameObject _floor;

	private Coroutine _spawnObstacleCoroutine;

	private int _size;

	private void Awake()
	{
		Instance = this;
	}

	public void SetSize(int size)
	{
		if (_tiles != null)
		{
			DestroyMap();
		}
		_size = size;
		Initialize();

	}

	public void ShowMap(bool show)
	{
		_parentObject.gameObject.SetActive(show);
	}

	public void Initialize()
	{
		// reset map position
		_parentObject.position = Vector3.zero;
		_tiles = new Tile[_size, _size];
		for (int i = 0; i < _size; i++)
		{
			for (int j = 0; j < _size; j++)
			{
				var tile = ((GameObject) Instantiate(_floor, new Vector3(i, 0, j),
					Quaternion.identity)).GetComponent<Tile>();
				_tiles[i, j] = tile;
				tile.CachedTransform = tile.GetComponent<Transform>();
				tile.Animation = tile.GetComponent<Animation>();
				tile.GetComponentInChildren<Renderer>().sharedMaterial = GetCheckerboardTile(i, j);
				tile.CachedTransform.parent = _parentObject;
				if (!GameControl.Instance.MarkerDetected)
				{
					tile.GetComponentInChildren<Renderer>().enabled = false;
				}
				// set borders
				if (i == 0 || j == 0 || i == _size - 1 || j == _size - 1)
				{
					tile.CachedTransform.GetChild(0).localPosition = Vector3.zero;
					tile.TileState = TileStateEnum.Busy;
				}
			}
		}
		// move map to middle of scene
		_parentObject.position = new Vector3(-_size/2f, 0, -_size/2f);
		_parentObject.gameObject.SetActive(false);
	}

	public void ActivateObstacles()
	{
		_spawnObstacleCoroutine = StartCoroutine(SpawnObstacleCoroutine());
	}

	private IEnumerator SpawnObstacleCoroutine()
	{
		while (true)
		{
			for (int i = 0; i < Random.Range((int)(_tiles.Length * 0.02), (int)(_tiles.Length * 0.05)); i++)
			{
				var tile = GetFreeTileOnMinDistance(SnakeControl.Instance.Head.CachedTransform.position, 3);
				tile.TileState = TileStateEnum.Attention;
				tile.Animation.Play();
			}
			yield return new WaitForSeconds(10);
		}
		
	}

	public void StopSpawnObstacles()
	{
		if (_spawnObstacleCoroutine != null)
		{
			StopCoroutine(_spawnObstacleCoroutine);
		}
	}

	public void ResetMap()
	{
		for (int i = 1; i < _size - 1; i++)
		{
			for (int j = 1; j < _size - 1; j++)
			{
				_tiles[i, j].Bonus = null;
				_tiles[i, j].TileState = TileStateEnum.Clear;
				_tiles[i, j].Animation.Stop();
				_tiles[i, j].ResetPos();
			}
		}
	}

	private void DestroyMap()
	{
		for (int i = 0; i < _size; i++)
		{
			for (int j = 0; j < _size; j++)
			{
				Destroy(_tiles[i, j].gameObject);
			}
		}
		_tiles = null;
	}


	private Material GetCheckerboardTile(int x, int y)
	{
		if (x % 2 == 0)
		{
			return y % 2 == 0 ? _oddBlockMaterial : _evenBlockMaterial;
		}
		else
		{
			return y % 2 == 0 ? _evenBlockMaterial : _oddBlockMaterial;
		}
	}

	public CurrentPosition GetMiddleOfMap()
	{
		return new CurrentPosition(_size / 2, _size / 2);
	}

	public TileStateEnum CheckTileAvailability(CurrentPosition pos)
	{
		if (pos.X >= _size || pos.X < 0 || pos.Z >= _size || pos.Z < 0)
		{
			return TileStateEnum.Void;
		}
		return _tiles[pos.X, pos.Z].TileState;
	}

	public Vector3 TakePosition(CurrentPosition pos)
	{
		_tiles[pos.X, pos.Z].TileState = TileStateEnum.Busy;
		return _tiles[pos.X, pos.Z].CachedTransform.position;
	}

	public void FreePosition(CurrentPosition pos)
	{
		_tiles[pos.X, pos.Z].TileState = TileStateEnum.Clear;
	}

	public Tile GetTile(CurrentPosition pos)
	{
		return _tiles[pos.X, pos.Z];
	}

	public Tile GetFreeTile()
	{
		while (true)
		{
			var tile = _tiles[Random.Range(0, _size), Random.Range(0, _size)];
			if (tile.TileState == TileStateEnum.Clear)
			{
				return tile;
			}
		}
	}

	public Tile GetFreeTileOnMinDistance(Vector3 currPos, float minSqrDistance)
	{
		while (true)
		{
			var tile = _tiles[Random.Range(0, _size), Random.Range(0, _size)];
			if (tile.TileState == TileStateEnum.Clear && 
				RangeUtility.CalcDistance(tile.CachedTransform.position, currPos) > minSqrDistance)
			{
				return tile;
			}
		}
	}
}
