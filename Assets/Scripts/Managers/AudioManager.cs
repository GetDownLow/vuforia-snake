﻿using System;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class AudioManager : MonoBehaviour
{
	public static AudioManager Instance;

	private AudioLibrary library;
	private AudioSource fxSource;
	private AudioSource musicSource;

	[SerializeField]
	private AudioClip _mainMusic;

	private int currMusicIndex;

	void Awake()
	{
		Instance = this;
		var audio = GetComponents<AudioSource>();
		fxSource = audio[0];
		musicSource = audio[1];
		library = GetComponent<AudioLibrary>();
		musicSource.clip = _mainMusic;
		musicSource.Play();
	}

	public void PlayConstantSound(string id)
	{
		fxSource.PlayOneShot(library.GetSound(id));
	}
}
