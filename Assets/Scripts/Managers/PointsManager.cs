﻿using UnityEngine;

class PointsManager : MonoBehaviour
{
	public static PointsManager Instance;

	private const string ScoreString = "Highscore";

	private int _currentPoints;

	private int _highscore;

	private void Awake()
	{
		Instance = this;
		_highscore = PlayerPrefs.GetInt(ScoreString);
		UIManager.Instance.SetMenuHighscore(_highscore);
	}

	public void ResetScore()
	{
		_currentPoints = 0;
	}


	public void ShowFinalPoints()
	{
		if (_currentPoints > _highscore)
		{
			_highscore = _currentPoints;
			PlayerPrefs.SetInt(ScoreString, _highscore);
			UIManager.Instance.SetMenuHighscore(_highscore);
		}
		UIManager.Instance.FinalScore(_currentPoints, _highscore);
	}

	public void AddPoint(int count)
	{
		_currentPoints += count;
		UIManager.Instance.SetPoints(_currentPoints);
	}

	private void CountHighscore()
	{

	}
}

