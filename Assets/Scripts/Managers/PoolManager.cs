﻿using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
	public static PoolManager Instance;

	[SerializeField]
	private Transform _parent;

	// array of _pools
	[SerializeField]
	private Pool[] _pools;

	private void Awake()
	{
		Instance = this;
		for (var i = 0; i < _pools.Length; i++)
		{
			initializePool(_pools[i]);
		}
	}

	private void initializePool(Pool pool)
	{
		for (var i = 0; i < pool.Count; i++)
		{
			AddItem(pool);
		}
	}

	private GameObject AddItem(Pool pool, bool deactivate = true)
	{
		var obj = Instantiate(pool.Object);
		if (deactivate)
		{
			obj.SetActive(false);
		}
		obj.transform.SetParent(_parent);
		pool.ObjList.Add(obj);
		return obj;
	}

	private void SetPosition(Transform obj, Vector3 pos, Quaternion rot)
	{
		obj.position = pos;
		obj.rotation = rot;
	}

	public GameObject Pop(int poolInt, Vector3 pos, Quaternion rot)
	{
		for (var i = 0; i < _pools[poolInt].ObjList.Count; i++)
		{
			if (!_pools[poolInt].ObjList[i].activeInHierarchy)
			{
				SetPosition(_pools[poolInt].ObjList[i].transform, pos, rot);
				_pools[poolInt].ObjList[i].SetActive(true);
				return _pools[poolInt].ObjList[i];
			}
		}
		var obj = AddItem(_pools[poolInt], false);
		SetPosition(obj.transform, pos, rot);
		return obj;
	}

	public void PushAll()
	{
		for (var i = 0; i < _pools.Length; i++)
		{
			for (var j = 0; j < _pools[i].ObjList.Count; j++)
			{
				if (_pools[i].ObjList[j].activeInHierarchy)
				{
					_pools[i].ObjList[j].SetActive(false);
				}
			}
		}
	}
}

[System.Serializable]
public class Pool
{
	public GameObject Object;
	public int Count;
	[HideInInspector]
	public List<GameObject> ObjList = new List<GameObject>();
}
