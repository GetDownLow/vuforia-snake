﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour
{
	public static SettingsManager Instance;
	
	[SerializeField]
	private Slider _speedSlider;

	[SerializeField]
	private Slider _sizeSlider;

	[SerializeField]
	private Text _speedText;

	[SerializeField]
	private Text _sizeText;

	private const string SpeedString = "SnakeSpeed";

	private const string SizeString = "SizeMapString";

	private const string Speed = "Speed {0}";

	private const string MapSize = "Map size {0}";

	private int _currentSpeed;

	private int _currentSize;

	private void Awake()
	{
		Instance = this;
		_speedSlider.onValueChanged.AddListener(OnSpeedChange);
		_sizeSlider.onValueChanged.AddListener(OnSizeChange);
		SetStartVariables();
	}

	private void OnSpeedChange(float value)
	{
		_currentSpeed = (int) value;
		_speedText.text = StringController.Format(Speed, _currentSpeed);
	}

	private void OnSizeChange(float value)
	{
		_currentSize = (int)value;
		_sizeText.text = StringController.Format(MapSize, _currentSize);
	}

	private void SetStartVariables()
	{
		_speedSlider.value = _currentSpeed = PlayerPrefs.GetInt(SpeedString, 5);
		_sizeSlider.value = _currentSize = PlayerPrefs.GetInt(SizeString, 15);
		_speedText.text = StringController.Format(Speed, _currentSpeed);
		_sizeText.text = StringController.Format(MapSize, _currentSize);
		SnakeControl.Instance.SetSpeed(_currentSpeed);
		MapManager.Instance.SetSize(_currentSize);
	}

	public void ApplyChanges()
	{
		if (PlayerPrefs.GetInt(SpeedString, 4) != _currentSpeed)
		{
			PlayerPrefs.SetInt(SpeedString, _currentSpeed);
			SnakeControl.Instance.SetSpeed(_currentSpeed);
		}
		if (PlayerPrefs.GetInt(SizeString, 15) != _currentSize)
		{
			PlayerPrefs.SetInt(SizeString, _currentSize);
			MapManager.Instance.SetSize(_currentSize);
		}
	}
}
