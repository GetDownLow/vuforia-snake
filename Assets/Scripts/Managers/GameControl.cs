﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Vuforia;

public class GameControl : MonoBehaviour, ITrackableEventHandler
{
	public static GameControl Instance;

	[HideInInspector]
	public bool MarkerDetected;

	[HideInInspector]
	public GameStateEnum GameState;

	private Renderer[] _rendererComponents;
	private TrackableBehaviour _mTrackableBehaviour;

	[SerializeField]
	private GameObject _target;

	private void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
		_mTrackableBehaviour = _target.GetComponent<TrackableBehaviour>();
		if (_mTrackableBehaviour)
		{
			_mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
	}

	public void StartGame()
	{
		UIManager.Instance.ChangeGameState(true);
		MapManager.Instance.ShowMap(true);
		SnakeControl.Instance.InitializeComponents();
		CollectableManager.Instance.Spawn();
		PointsManager.Instance.ResetScore();
		Time.timeScale = 0;
	}

	public void GameOver()
	{
		GameState = GameStateEnum.Menu;
		PointsManager.Instance.ShowFinalPoints();
		AudioManager.Instance.PlayConstantSound("GameOver");
		MapManager.Instance.StopSpawnObstacles();
	}

	public void EndGame()
	{
		UIManager.Instance.ChangeGameState(false);
		MapManager.Instance.ShowMap(false);
		GameState = GameStateEnum.Menu;
		PoolManager.Instance.PushAll();
		MapManager.Instance.StopSpawnObstacles();
		MapManager.Instance.ResetMap();
		Time.timeScale = 1;
	}

	public void Pause(bool pause)
	{
		UIManager.Instance.Pause(pause);
		if (pause)
		{
			GameState = GameStateEnum.Pause;
			Time.timeScale = 0;
		}
		else
		{
			GameState = GameStateEnum.Game;
			Time.timeScale = MarkerDetected ? 1 : 0;
		}
	}

	public void Restart()
	{
		PoolManager.Instance.PushAll();
		MapManager.Instance.ResetMap();
		PointsManager.Instance.ResetScore();
		StartGame();
		
	}

	public void Exit()
	{
		Application.Quit();
	}


	#region Trackable
	public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED || 
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			OnTrackingFound();
		}
		else
		{
			OnTrackingLost();
		}
	}

	private void OnTrackingFound()
	{
		_rendererComponents = _target.GetComponentsInChildren<Renderer>(true);
		for (int i = 0; i < _rendererComponents.Length; i++)
		{
			_rendererComponents[i].enabled = true;
		}
		MarkerDetected = true;
		if (GameState == GameStateEnum.Game)
		{
			Time.timeScale = 1;
		}
	}

	private void OnTrackingLost()
	{
		_rendererComponents = _target.GetComponentsInChildren<Renderer>(true);
		for (int i = 0; i < _rendererComponents.Length; i++)
		{
			_rendererComponents[i].enabled = false;
		}
		MarkerDetected = false;
		if (GameState == GameStateEnum.Game)
		{
			Time.timeScale = 0;
		}
	}
	#endregion
}
