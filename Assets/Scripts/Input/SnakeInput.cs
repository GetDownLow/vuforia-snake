﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class SnakeInput : MonoBehaviour
{
	private float _middleOfScreen;

	private EventSystem _evSys;

	private void Awake()
	{
		_middleOfScreen = Screen.width / 2f;
		_evSys = EventSystem.current;
	}

	void Update ()
	{
		if (GameControl.Instance.GameState != GameStateEnum.Game)
		{
			return;
		}
#if UNITY_EDITOR
		if (Input.GetMouseButtonDown(0))
		{
			SnakeControl.Instance.Turn(Input.mousePosition.x < _middleOfScreen);
		}
#elif UNITY_IPHONE || UNITY_ANDROID
		for (int i = 0; i < Input.touchCount; i++)
		{
			if (Input.GetTouch(i).phase == TouchPhase.Began && !_evSys.IsPointerOverGameObject(Input.GetTouch(i).fingerId))
			{
				SnakeControl.Instance.Turn(Input.mousePosition.x < _middleOfScreen);
			}
		}
#endif
	}
}
