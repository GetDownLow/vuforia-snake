﻿using UnityEngine;
using System.Collections;

public interface ISnake
{
	Transform CachedTransform { get; set; }

	CurrentPosition CurrentPos { get; set; }

	MeshFilter Mesh { get; set; }
}
