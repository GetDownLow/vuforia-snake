﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	public static UIManager Instance;

	[SerializeField]
	private Text _pointsText;

	[SerializeField]
	private Animation _pointsAnimation;

	[SerializeField]
	private GameObject _gameInfo;

	[SerializeField]
	private GameObject _mainMenu;

	[SerializeField]
	private GameObject _gameUI;

	[SerializeField]
	private Text _finalScore;

	[SerializeField]
	private Text _finalHighscore;

	[SerializeField]
	private Text _menuHighscore;

	[SerializeField]
	private GameObject _finalPanel;

	[SerializeField]
	private GameObject _pauseScreen;

	public const string Zero = "0";

	private const string ScoreString = "Score {0}";

	private const string HighscoreString = "Highscore {0}";

	private void Awake ()
	{
		Instance = this;
	}
	
	public void SetPoints(int points)
	{
		_pointsText.text = points.ToString();
		_pointsAnimation.Play();
	}

	private void ResetScore()
	{
		_pointsText.text = Zero;
	}

	public void Pause(bool pause)
	{
		_pauseScreen.SetActive(pause);
		_gameUI.SetActive(!pause);
	}

	public void SetMenuHighscore(int count)
	{
		_menuHighscore.text = StringController.Format(HighscoreString, count);
	}

	public void FinalScore(int score, int highscore)
	{
		_finalScore.text = StringController.Format(ScoreString, score);
		_finalHighscore.text = StringController.Format(HighscoreString, highscore);
		_finalPanel.SetActive(true);
		_gameUI.SetActive(false);
	}

	public void ChangeGameState(bool game)
	{
		_mainMenu.SetActive(!game);
		_gameUI.SetActive(false);
		if (game)
		{
			_gameInfo.SetActive(true);
			ResetScore();
		}
	}

	public void SkipInfo()
	{
		Time.timeScale = GameControl.Instance.MarkerDetected ? 1 : 0;
		MapManager.Instance.ActivateObstacles();
		GameControl.Instance.GameState = GameStateEnum.Game;
		AudioManager.Instance.PlayConstantSound("Start");
		_gameUI.SetActive(true);
		_gameInfo.SetActive(false);
		_pointsAnimation.Play();
	}
}
